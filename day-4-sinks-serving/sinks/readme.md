### Sink connector




### apply yaml files into k8s
```sh
# deploy yaml files
k apply -f day-4-sinks-serving/sinks/elasticsearch/sink-elastic-app-movies-keywords-json.yaml -n ingestion
k apply -f day-4-sinks-serving/sinks/elasticsearch/sink-elastic-app-movies-titles-json.yaml -n ingestion
k apply -f day-4-sinks-serving/sinks/minio/sink-minio-output-pyspark-counts-country-batch-json.yaml -n ingestion
k apply -f day-4-sinks-serving/sinks/minio/sink-minio-output-pyspark-counts-genres-stream-json.yaml -n ingestion
k apply -f day-4-sinks-serving/sinks/mongodb/sink-mongodb-output-faust-enriched-rides-json.yaml -n ingestion
k apply -f day-4-sinks-serving/sinks/yugabytedb/sink-yugabytedb-ysql-enriched-ksqldb-stream-pr-musics-analysis-avro.yaml -n ingestion
k apply -f day-4-sinks-serving/mdw/bigquery/sink-bq-enriched-ksqldb-stream-pr-musics-analysis-avro.yaml -n ingestion
```
