
### Apache Pinot

In this document we can create tables for realtime ingestion with Apache Kafka and offline tables using MinIO, this covers:
* copy files into container
* execute admin command

##### Deploy tasks

```sh
# copy files from repository to [container] to build spec

# users kafka
k cp realtime_output_faust_enriched_rides_events_training.json datastore/pinot-controller-0:/opt/pinot
k cp sch_output_faust_enriched_rides_training.json datastore/pinot-controller-0:/opt/pinot


# access the admin tools logging into [controller] container
k exec pinot-controller-0 -i -t -- bash
/opt/pinot

# execute inside the container
JAVA_OPTS=""



# create table kafka users
bin/pinot-admin.sh AddTable \
-schemaFile /opt/pinot/sch_output_faust_enriched_rides_training.json \
-tableConfigFile /opt/pinot/realtime_output_faust_enriched_rides_events_training.json \
-exec


# get tasks logs
cat pinotController.log

```
