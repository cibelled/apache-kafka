"""
filename = gunicorn.py
name = configuration

description: gunicorn configuration
"""

host = "0.0.0.0"
port = "5000"
debug = True