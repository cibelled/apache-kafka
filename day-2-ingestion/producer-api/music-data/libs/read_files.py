"""
filename: read_files.py
name: read_files

description:
this is the function responsible to read files and perform some enhancements on the data.
use pandas and numpy to read data from a csv file and format to a dictionary
"""

# import libraries
import pandas as pd
import numpy as np
from configs import config

# pandas config
pd.set_option('display.max_rows', 100000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


class CSV:

    def __init__(self):
        self.music_file_location = config.music_file_location

    def csv_reader(self, gen_dt_rows):

        # reading files
        get_music_data = pd.read_csv(self.music_file_location)

        # fixing column names
        get_music_data.columns = get_music_data.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(','').str.replace(')', '')

        # replace nan to none
        get_music_data = get_music_data.replace({np.nan: None})

        # add new column [identity] = [0,1000]
        get_music_data['user_id'] = np.random.randint(0, 12900, size=(len(get_music_data), 1))

        # select column ordering
        df = get_music_data[['user_id', 'genre', 'artist_name', 'track_name', 'track_id', 'popularity', 'duration_ms']].head(gen_dt_rows)

        # convert to dictionary
        df_dict = df.to_dict('records')

        return df_dict