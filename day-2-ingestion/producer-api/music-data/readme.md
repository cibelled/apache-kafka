# Python Kafka Producer
> the python kafka producer is responsible to simulate events coming from a music app  
> which contains user music data. data is send to a kafka topic. this producer gives
> the ability to choose how many events will be generated per batch delivery.

> [Confluent Python Client for Kafka](https://github.com/confluentinc/confluent-kafka-python)  


## local environment [venv] setup
> this is a manual that shows how to set up a virtual environment
> to install the requested libraries to run the program

````sh
# install pip
pip3 install virtualenv

# check version
virtualenv --version

# path where program resides
/Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data

# create virtual env
virtualenv venv  

# activate env
source venv/bin/activate

# install requisites  
pip install -r requirements.txt  

# execute program
python3.8 app.py
curl -i http://localhost:5000/kafka?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&topic='src-app-music-data-json'&events=10
curl -i http://localhost:5000/confluent?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&schema_registry='http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081'&topic='src-app-music-data-avro'&events=10

# leave virtual environment
deactivate  

# delete env if necessary
rm -rf venv
````  

### step 1 - dockerize app
> create a dockerfile and containerize application

````sh
# list local images
docker images

# navigate to local
/Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data
Dockerfile

# build app
docker build --tag python-producer-music-app .

# open container
docker run -i -t python-producer-music-app /bin/bash

# run app
docker run python-producer-music-app
docker run python-producer-music-app python3.7 main.py

# remove dangling images
docker rmi $(docker images --filter "dangling=true" -q --no-trunc) -f
````

### step 2 - docker hub [registry]
> upload code written into a container registry - docker hub

```sh
# login
docker login

# docker hub
https://hub.docker.com/repositories

# tag image
docker tag python-producer-music-app owshq/python-producer-music-app:2.1

# images
docker images
owshq/python-producer-music-app:2.1

# push image to registry
docker push owshq/python-producer-music-app:2.1
```

### step 3 - deploy application on kubernetes [k8s]
> deploying app into kubernetes cluster

```sh
# location
/Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data/deployment/deployment.yaml

# deploy app
k apply -f /Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data/deployment/deployment.yaml -n app

# deployment info
k describe deployment python-producer-music

# list & describe pods
k get pods -l name=app-music-flaskapi
k describe pod -l name=app-music-flaskapi

# verify logs
k logs -l name=app-music-flaskapi -f

# remove deployment
k delete deployment python-producer-music
```

### step 4 - test app deployment
> log into container and test, deploy service to expose app

```sh
# access container
k exec -c app-music-flaskapi -it python-producer-music-55f4dc46c-2h8nd -- /bin/bash

# service
/Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data/deployment/service.yaml

# deploy service
k apply -f /Users/luanmorenomaciel/BitBucket/apache-kafka/4-producer-api/confluent-kafka-python/music-data/deployment/service.yaml

# describe service
k describe service app-python-producer-music-aks-lb

# remove service
k delete service app-python-producer-music-aks-lb
```

### step 5 - send data to kafka [test]
> test application using http requests

```sh
# describe load balancer
k describe svc app-python-producer-music-aks-lb
40.70.154.93

# flask http requests [dev-kafka-confluent.eastus2.cloudapp.azure.com]
http://40.70.154.93/kafka?broker=dev-kafka-confluent.eastus2.cloudapp.azure.com&topic=src-app-music-data-json&events=10
http://40.70.154.93/confluent?broker=dev-kafka-confluent.eastus2.cloudapp.azure.com&schema_registry=http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081&topic=src-app-music-data-avro&events=10

# curl requests
curl -i http://40.70.154.93/kafka?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&topic='src-app-music-data-json'&events=10
curl -i http://40.70.154.93/confluent?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&schema_registry='http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081'&topic='src-app-music-data-avro'&events=10
```
