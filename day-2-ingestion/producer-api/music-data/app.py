"""
python3.8 main.py stream

main flask application that sends data to either kafka or confluent broker

python3.8 app.py
"""

# import libraries
import config
from libs import sr_confluent_kafka
from libs import json_kafka
from flask import Flask, render_template, request, jsonify

# flask app
app = Flask(__name__)

# http://0.0.0.0:8080/
@app.route("/")
def home():
    return render_template("home.html")

# http://0.0.0.0:8080/about
@app.route("/about")
def about():
    return render_template("about.html")

#####################################
# apache kafka
#####################################
@app.route('/kafka')
def kafka():
    """
    broker = 'dev-kafka-confluent.eastus2.cloudapp.azure.com'
    topic = 'src-app-music-data-json'
    events = 100

    curl -i http://localhost:8080/kafka?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&topic='src-app-music-data-json'&events=10
    http://0.0.0.0:8080/kafka?broker=dev-kafka-confluent.eastus2.cloudapp.azure.com&topic=src-app-music-data-json&events=10
    """
    broker = request.args.get('broker')
    topic = request.args.get('topic')
    events = request.args.get('events')
    json_kafka.KafkaProducer().json_producer(broker, topic, int(events))
    return jsonify({'result': 'events processed successfully, ' + events})

#####################################
# confluent
#####################################
@app.route('/confluent')
def confluent():
    """
    broker = 'dev-kafka-confluent.eastus2.cloudapp.azure.com'
    schema_registry_url = 'http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081'
    topic = 'src-app-music-data-avro'
    events = 100

    curl -i http://localhost:8080/confluent?broker='dev-kafka-confluent.eastus2.cloudapp.azure.com'&schema_registry='http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081'&topic='src-app-music-data-avro'&events=10
    http://0.0.0.0:8080/confluent?broker=dev-kafka-confluent.eastus2.cloudapp.azure.com&schema_registry=http://dev-kafka-confluent.eastus2.cloudapp.azure.com:8081&topic=src-app-music-data-avro&events=10
    """
    broker = request.args.get('broker')
    schema_registry = request.args.get('schema_registry')
    topic = request.args.get('topic')
    events = request.args.get('events')
    sr_confluent_kafka.Kafka().avro_producer(broker, schema_registry, topic, int(events))
    return jsonify({'result': 'events processed successfully, ' + events})


# main
if __name__ == "__main__":
    # init http server
    # flask api + wsgi
    app.run(debug=True, host=config.host, port=config.port)




