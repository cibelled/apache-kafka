"""
filename: mongodb.py
name: mongodb

description:
this area is responsible to interact with mongodb [no sql] database
"""

# import libraries
import pymongo
import config
import read_files


class MongoDB:

    @staticmethod
    def insert_document(gen_dt_rows):

        # build connection string [conn string]
        connection_string = config.cs

        # initiate connection
        client = pymongo.MongoClient(connection_string)

        # get database [music] and collection [user_data]
        database = client.get_database('music')
        collection = pymongo.collection.Collection(database, 'user_data')

        # read user_data to insert as a document
        # select amount of documents to insert
        document = read_files.CSV().csv_reader(gen_dt_rows)

        # get user_id from new document
        find_user_id_document = [document['user_id'] for document in document]
        # print("user_id's received for insertion:", find_user_id_document)

        # retrieve user_id
        find_user_id_mongodb = [document['user_id'] for document in collection.find({}, {"user_id"})]
        print("user_id's registered into mongodb already:", find_user_id_mongodb)

        # check if list is empty
        if len(find_user_id_mongodb) == 0:

            # insert the document into collection
            document_insert = collection.insert_many(document)
            print(document_insert.inserted_ids)

        else:
            # get last element of the list
            last_element = find_user_id_mongodb[-1]
            print("last inserted document:", last_element)

            # verify user_id to be inserted as [document] into [mongodb]
            get_user_id = set(find_user_id_document) - set(find_user_id_mongodb)
            print("list of user_id's for insert:", list(get_user_id))

            # getting correct documents to insert into mongodb
            # listing only not inserted documents
            get_data_insert = [x for x in document if x['user_id'] in get_user_id]

            if get_data_insert != []:
                # insert document [dictionary] into mongodb
                # insert_many()
                document_insert = collection.insert_many(get_data_insert)
                print(document_insert.inserted_ids)
