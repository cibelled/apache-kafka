"""
cli to insert data into mongodb no sql database

python3.8 main.py
python3.8 main.py 'mongodb' 10
"""

# import libraries
import mongodb
import argparse
import sys

# main
if __name__ == '__main__':

    # instantiate arg parse
    parser = argparse.ArgumentParser(description='streaming app to mongodb')

    # add parameters to arg parse
    parser.add_argument('nosql database', choices=['mongodb'], type=str, help='nosql database')
    parser.add_argument('gen_dt_rows', type=int, help='amount of events to insert')

    # invoke help [if null]
    args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

    # [cli call]

    # main
    mongodb.MongoDB().insert_document(args.gen_dt_rows)
