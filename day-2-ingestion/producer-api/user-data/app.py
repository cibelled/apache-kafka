"""
python3.8 main.py stream

main flask application that sends data to mongodb

python3.8 app.py
"""

# import libraries
import config
import mongodb
from flask import Flask, render_template, request, jsonify, make_response

# flask app
app = Flask(__name__)

# http://0.0.0.0:8080/
@app.route("/")
def home():
    return render_template("home.html")

# http://0.0.0.0:8080/about
@app.route("/about")
def about():
    return render_template("about.html")

#####################################
# mongodb
#####################################
@app.route('/mongodb')
def kafka():
    """
    curl -i http://localhost:7900/mongodb?document=10
    """

    document = request.args.get('document')
    mongodb.MongoDB().insert_document(int(document))
    return jsonify({'result': 'document, ' + document})


# main
if __name__ == "__main__":
    # init http server
    # flask api + wsgi
    app.run(debug=True, host=config.host, port=config.port)