# KSQLDB ~ SQL Streaming for Apache Kafka

## install ksqldb
```sh
# use cluster
kubectx aks-owshq-event-stream

# create namespace
k create namespace processing

# install ksqldb yaml files
k apply -f day-3-processors/ksqldb/yamls/ -n processing

# get info
kubens processing
kgpw
```

### working with ksqldb
```sh
# access ksqldb server
KSQLDB=ksqldb-server-bf846f4c6-s7crl
k exec $KSQLDB -n processing -i -t -- bash ksql

# set latest offset read
SET 'auto.offset.reset' = 'earliest';
SET 'auto.offset.reset' = 'latest';

# show info
SHOW TOPICS;
SHOW STREAMS;
SHOW TABLES;
SHOW QUERIES;
```

##### create streams from topics [avro]
```sh
# generate events using ingestion app
# verify schema registry
# verify load balancer
export KAFKA_SCHEMA_REGISTRY = "http://20.75.59.189:8081"

# bash file = batch_ksqldb_events.bash
python3.9 cli.py 'strimzi-musics-avro'
python3.9 cli.py 'strimzi-credit-card-avro'
python3.9 cli.py 'strimzi-agent-avro'
python3.9 cli.py 'strimzi-users-avro'

# avro streams
CREATE OR REPLACE STREAM ksql_stream_app_musics_avro WITH (KAFKA_TOPIC='src-app-musics-avro', VALUE_FORMAT='AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_credit_card_avro WITH (KAFKA_TOPIC='src-app-credit-card-avro', VALUE_FORMAT='AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_agent_avro WITH (KAFKA_TOPIC='src-app-agent-avro', VALUE_FORMAT='AVRO');
CREATE OR REPLACE STREAM ksql_stream_app_users_avro WITH (KAFKA_TOPIC='src-app-users-avro', VALUE_FORMAT='AVRO');

SHOW STREAMS;
```

##### queries stream avro objects
```sql
SELECT *
FROM KSQL_STREAM_APP_MUSICS_AVRO
EMIT CHANGES LIMIT 10;

SELECT *
FROM KSQL_STREAM_APP_AGENT_AVRO
EMIT CHANGES LIMIT 10;

SELECT *
FROM KSQL_STREAM_APP_CREDIT_CARD_AVRO
EMIT CHANGES LIMIT 10;

SELECT *
FROM KSQL_STREAM_APP_USERS_AVRO
EMIT CHANGES LIMIT 10;

SELECT * 
FROM ENRICHED_KSQLDB_STREAM_PR_MUSIC_ANALYSIS_AVRO
EMIT CHANGES LIMIT 10;
```

### housekeeping
```sh
# drop enriched stream
TERMINATE CSAS_ENRICHED_KSQLDB_STREAM_PR_MUSIC_ANALYSIS_AVRO_53;
DROP STREAM ENRICHED_KSQLDB_STREAM_PR_MUSIC_ANALYSIS_AVRO;

# drop keyed streams
TERMINATE CSAS_KSQL_STREAM_APP_MUSICS_AVRO_KEYED_45;
TERMINATE CSAS_KSQL_STREAM_APP_CREDIT_CARD_AVRO_KEYED_49;
TERMINATE CSAS_KSQL_STREAM_APP_AGENT_AVRO_KEYED_47;
TERMINATE CSAS_KSQL_STREAM_APP_USERS_AVRO_KEYED_51;

DROP STREAM KSQL_STREAM_APP_MUSICS_AVRO_KEYED;
DROP STREAM KSQL_STREAM_APP_CREDIT_CARD_AVRO_KEYED;
DROP STREAM KSQL_STREAM_APP_AGENT_AVRO_KEYED;
DROP STREAM KSQL_STREAM_APP_USERS_AVRO_KEYED;

# drop streams
DROP STREAM ksql_stream_app_musics_avro;
DROP STREAM ksql_stream_app_credit_card_avro;
DROP STREAM ksql_stream_app_agent_avro;
DROP STREAM ksql_stream_app_users_avro;
```

### dropping topics [kafka]
```sh
# drop kafka topics
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic ksqldb-stream-agent-avro-keyed
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic ksqldb-stream-credit-card-avro-keyed
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic ksqldb-stream-musics-avro-keyed
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic ksqldb-stream-users-avro-keyed
kubectl exec edh-kafka-0 -c kafka -i -t -- bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic enriched-ksqldb-stream-pr-musics-analysis-avro
```
