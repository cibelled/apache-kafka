### Stream-table Duality

In many document and books we see close relationship between ksqldb stream and ksqldb tables, now we going to explore this Duality


#### Creating KSQLDB Objects

create a table, primary key is need, the topic already have a key key_format = avro because the topic is in avro from kafka connector
```sql
CREATE TABLE ksqldb_table_mysql_device_avro  WITH  (KAFKA_TOPIC = 'owshq-mysql.owshq.device',  VALUE_FORMAT = 'AVRO', KEY_FORMAT = 'AVRO');
```

create a stream, primary key is not need, we can create from topic, in this case we use the same as used for table, avro topic no need to declare columns in sintax
```sql
CREATE STREAM ksqldb_stream_mysql_device_avro  WITH  (KAFKA_TOPIC = 'owshq-mysql.owshq.device',  VALUE_FORMAT = 'AVRO');

```

#### Exploring duality between stream and table

In the end of the day streams are like tables, the same concept but they are more for changelog for example, in this case we have a CDC Topic and we can see what happen when I make insert, update and delete operations.

```sql

SELECT
"BEFORE"->"ID" as "before_id",
"AFTER"->"ID" as "after_id",
"BEFORE"->"MODEL" as "before_model",
"AFTER"->"MODEL" as "after_model",
"BEFORE"->"MANUFACTURER" as "before_manufacturer",
"AFTER"->"MANUFACTURER" as "after_manufacturer",
"BEFORE"->"USER_ID" as "before_user_id",
"AFTER"->"USER_ID" as "after_user_id",
"BEFORE"->"DT_CURRENT_TIMESTAMP" as "before_dt_current_timestamp",
"AFTER"->"DT_CURRENT_TIMESTAMP" as "after_dt_current_timestamp"
FROM KSQLDB_STREAM_MYSQL_DEVICE_AVRO
EMIT CHANGES;

```

Now let's work with the tables, normally we use tables when we want to save aggregation for example, COUNT:


```sql
-- count with after manufacturer [work in each],  write the last state
SELECT
COUNT ("AFTER"->"MANUFACTURER") as "count_after_manufacturer", "AFTER"->"MANUFACTURER" as "after_manufacturer"
FROM KSQLDB_TABLE_MYSQL_DEVICE_AVRO
GROUP by "AFTER"->"MANUFACTURER"
EMIT CHANGES;

-- count with before manufacturer [need to update or delete the source mysql.device] write the last state
SELECT
COUNT ("BEFORE"->"MANUFACTURER") as "count_before_manufacturer", "BEFORE"->"MANUFACTURER" as "before_manufacturer"
FROM KSQLDB_TABLE_MYSQL_DEVICE_AVRO
WHERE "BEFORE"->"MANUFACTURER" IS NOT NULL
GROUP by "BEFORE"->"MANUFACTURER"
EMIT CHANGES;

```
