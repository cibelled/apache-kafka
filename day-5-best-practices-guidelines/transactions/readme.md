# Apache Kafka ~ Transactions

### eos and transactions info 
```
* exactly-once semantics - eos
- idempotent producer = without duplicates, no data loss & in-order guarantee

* transactions
- app pattern of read-process-write [approach] ~ async calls
- financial institutions use stream processing for debits and credits on user accounts [eos]
- support of atomic writes using the transactions api 
- producer and consumer apis are involved on the process
- for consumer = read_comitted and read_uncommited options available
- commit latest offset consumed is part of the internal process
- remove zombie fencing by adding transactional id to use across process restarts
- components - transaction coordinator [inside of kafka broker] & transaction log [internal topic] ~ __transaction-state
```

### transaction's api reference
```shell
https://www.confluent.io/blog/exactly-once-semantics-are-possible-heres-how-apache-kafka-does-it/
https://www.confluent.io/blog/transactions-apache-kafka/
https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/index.html#transactional-producer-api
https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
https://github.com/confluentinc/confluent-kafka-python/blob/master/examples/eos-transactions.py
```

### initiate application(s)
```shell
python3.9 main.py 'eos-producer-users-transactions'
```

### debugging and investigating behaviour
```shell
kubectl exec -ti edh-zookeeper-0 -- bin/kafka-topics.sh --list --zookeeper localhost:12181

kafkacat -C -b 20.75.59.77:9094 -t __transaction_state -J -o end

kafkacat -C -b 20.75.59.77:9094 -t users-eos-json -J -o end
 
kafkacat -C -b 20.75.59.77:9094 -t __consumer_offsets -J -o end
```