# import libraries
import os
import consumer_settings
from dotenv import load_dotenv
from confluent_kafka import Consumer, TopicPartition, KafkaError

# get env
load_dotenv()

# load variables
kafka_bootstrap_server = os.getenv("KAFKA_BOOTSTRAP_SERVER")
kafka_topic = os.getenv("KAFKA_SRC_TOPIC")

# init configs
# specify consumer parameters
c = Consumer(consumer_settings.consumer_settings_json(kafka_bootstrap_server))

# assign a topic to read
# specific partition if necessary
# fence out zombies

# c.assign([TopicPartition(kafka_topic, 0)])
c.subscribe([kafka_topic])

# loop over events
# thread safety = immutable state of a class
# shared state in a app using same thread to multiple readers
eof = {}
events_cnt = 0
while True:

    # poll = interval in [ms] to pull events
    # timeout in seconds to use async function to retrieve events
    events = c.poll(timeout=1.0)
    if events is None:
        continue

    # set variables
    # retrieve topic, partition, offset and events (data)
    topic, partition, offset, data = events.topic(), events.partition(), events.offset(), events.value().decode('utf-8')
    print(topic, partition, offset, data)

    if events.error():
        if events.error().code() == KafkaError._PARTITION_EOF:
            eof[(topic, partition)] = True
            print("=== reached the end of {} [{}] at {} ====".format(topic, partition, events.offset()))

        continue

    # clear eof if a new message has been received
    # increment counter for loop
    eof.pop((topic, partition), None)
    events_cnt += 1

# close call
# terminate consumer
c.close()
