# import libraries
import os
from dotenv import load_dotenv

# get env
load_dotenv()

# load variables
kafka_client_id_json = os.getenv("KAFKA_CLIENT_ID_JSON")
kafka_bootstrap_server = os.getenv("KAFKA_BOOTSTRAP_SERVER")


# [json] = producer config
# in this case if we comment enable.idempotence, acks and have more then 1 partition order is not guarantee
def producer_settings_json(broker):

    json = {
        'client.id': kafka_client_id_json,
        'bootstrap.servers': broker,
        "batch.num.messages": 100000,
        "linger.ms": 1000,
        "enable.idempotence": "true",
        "acks": "all",
        "max.in.flight.requests.per.connection": 1
        }

    # return data
    return dict(json)

