# import libraries
import os
from dotenv import load_dotenv

# get env
load_dotenv()

# load variables
kafka_consumer_client_id_json = os.getenv("KAFKA_CONSUMER_CLIENT_ID_JSON")
kafka_bootstrap_server = os.getenv("KAFKA_BOOTSTRAP_SERVER")
latest_message = os.getenv("KAFKA_OFFSET_LATEST_SETTINGS")
earliest_message = os.getenv("KAFKA_OFFSET_EARLIEST_SETTINGS")

# [json] = consumer config
def consumer_settings_json():

    json = {
        'bootstrap.servers': kafka_bootstrap_server,
        'group.id': kafka_consumer_client_id_json,
        'auto.commit.interval.ms': 6000,
        'auto.offset.reset': 'earliest'
        }

    # return data
    return dict(json)